package pro.belbix.codgen;

import org.junit.Test;
import pro.belbix.codgen.operation.MethodOp;
import pro.belbix.codgen.operation.Operation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class EnvContextTest {
    private EnvContext env = EnvContext.getInstance();

    @Test
    public void shouldAvoidRecursion() {
        MethodOp m1 = new MethodOp();
        env.addMethod(m1);

        MethodOp m2 = new MethodOp();
        env.addMethod(m2);

        m1.addOperation(m2);
        env.getRecursionChecker().updateLinks(m1, m2);

        Operation nextOp = env.getBestFor(m1, 2);
        assertNull("should avoid duplicate, because m1 already have m2", nextOp);

        nextOp = env.getBestFor(m2, 2);
        assertNull("should avoid recursion, because m1 linked to m2", nextOp);

        MethodOp m3 = new MethodOp();
        env.addMethod(m3);

        nextOp = env.getBestFor(m2, 2);
        assertEquals("should find m3", m3, nextOp);
        m2.addOperation(nextOp);
        env.getRecursionChecker().updateLinks(m2, m3);

        nextOp = env.getBestFor(m3, 2);
        assertNull("should avoid recursion", nextOp);
    }

    @Test
    public void multiple() {
        int c = 0;
        while (c < 1000) {
            env.clear();
            shouldAvoidRecursion();
            c++;
        }
    }
}