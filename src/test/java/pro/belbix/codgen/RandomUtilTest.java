package pro.belbix.codgen;

import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class RandomUtilTest {

    @Test
    public void randomInt() {

        assertEquals(0 ,RandomUtil.randomInt(0));
        int c = 0;
        Set<Integer> r = new HashSet<>();
        while (c < 100) {
            r.add(RandomUtil.randomInt(1));
            c++;
        }
        assertTrue(r.contains(0));
        assertTrue(r.contains(1));
        assertFalse(r.contains(2));

    }

    @Test
    public void test2() {
        int c = 0;
        Set<Integer> r = new HashSet<>();
        while (c < 10000) {
            r.add(RandomUtil.randomInt(3));
            c++;
        }
        assertTrue(r.contains(0));
        assertTrue(r.contains(1));
        assertTrue(r.contains(2));
        assertTrue(r.contains(3));
        assertFalse(r.contains(4));
        assertFalse(r.contains(5));
    }
}