package pro.belbix.codgen;

import org.junit.Test;
import pro.belbix.codgen.operation.MethodOp;

import static org.junit.Assert.assertTrue;

public class RecursionCheckerTest {
    private RecursionChecker recursionChecker = new RecursionChecker();


    @Test
    public void shouldFindRecursion() {
        MethodOp m1 = new MethodOp();
        MethodOp m2 = new MethodOp();
        MethodOp m3 = new MethodOp();

        addOp(m1, m2);
        addOp(m2, m3);

        assertTrue("should find recursion", recursionChecker.isRecursion(m3, m1));
    }

    private void addOp(MethodOp o, MethodOp f) {
        recursionChecker.isRecursion(o, f);
        o.addOperation(f);
        recursionChecker.updateLinks(o, f);
    }
}