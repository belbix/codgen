package pro.belbix.codgen;

import pro.belbix.codgen.operation.MethodOp;

public class CodGen {


    public static void main(String... args) throws InterruptedException {
        EnvContext.getInstance().initOps();
        MethodOp methodOp = new MethodOp();
        int count = 0;
        while (true) {
            count++;
            Object result = methodOp.doOp(new double[]{10, 1});
//            ResultHandler.handleResult(result, System.out::println);
//            System.out.println(new SourcePrinter().print(methodOp));
//            Thread.sleep(10000);
            System.out.println("----------------------------End " + count);
        }
    }


}
