package pro.belbix.codgen.operation;

import static pro.belbix.codgen.RandomUtil.randomInt;

public class LogicOp extends BasicOp {
    private final Logic logic;

    private Operation[] operations = new Operation[0];
    private double v1;
    private double v2;

    public LogicOp() {
        switch (randomInt(2)) {
            case 0:
                this.logic = Logic.LESS;
                break;
            case 1:
                this.logic = Logic.MORE;
                break;
            case 2:
                this.logic = Logic.EQUAL;
                break;
            default:
                throw new IllegalStateException();
        }
    }

    public LogicOp(Logic logic) {
        this.logic = logic;
    }

    @Override
    public Object handle(Object o) {
        operations = removeDead(operations);
        operations = addRandomOperation(operations);

        readInput(o);
        if (logic()) {
            Object[] results = new Object[operations.length];
            int i = 0;
            for (Operation op : operations) {
                results[i] = op.doOp(o);
                i++;
            }
            return results;
        }
        return null;
    }

    private void readInput(Object o) {
        if (o instanceof double[]) {
            double[] arr = (double[]) o;
            v1 = arr[0];
            v2 = arr[1];
        } else {
            throw new IllegalStateException("Unsupported type " + o.getClass());
        }
    }

    private boolean logic() {
        switch (logic) {
            case LESS:
                return v1 < v2;
            case MORE:
                return v1 > v2;
            case EQUAL:
                return v1 == v2;
            default:
                return false;
        }
    }

    public Logic getLogic() {
        return logic;
    }

    @Override
    public Operation[] getOperations() {
        return operations;
    }

    public void setOperations(Operation[] operations) {
        this.operations = operations;
    }

    public enum Logic {
        MORE(">"), LESS("<"), EQUAL("==");

        private final String value;

        Logic(String s) {
            this.value = s;
        }

        public String getValue() {
            return value;
        }
    }

}
