package pro.belbix.codgen.operation;

import pro.belbix.codgen.EnvContext;

import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;

import static pro.belbix.codgen.RandomUtil.randomInt;

public abstract class BasicOp implements Operation {
    private static final AtomicLong ID_GENERATOR = new AtomicLong(0);
    private final long id;
    private final Score score = new Score(randomInt(Integer.MAX_VALUE / 100));
    boolean dead = false;

    public BasicOp() {
        this.id = ID_GENERATOR.incrementAndGet();
    }

    @Override
    public long getId() {
        return id;
    }

    static Operation[] removeDead(Operation[] ops) {
        if (ops.length == 0) {
            return ops;
        }
        Operation[] result = new Operation[ops.length];
        int deleted = 0;
        for (int i = 0; i < ops.length; i++) {
            if (ops[i].isDead()) {
                deleted++;
            } else {
                result[i - deleted] = ops[i];
            }
        }
        return Arrays.copyOf(result, ops.length - deleted);
    }

    abstract Object handle(Object o);

    @Override
    public Score getScore() {
        return score;
    }

    @Override
    public void markDead() {
        dead = true;
    }

    @Override
    public boolean isDead() {
        return dead;
    }

    @Override
    public Object doOp(Object o) {
        try {
            Object r = handle(o);
            score.incrementScore(); //TODO not the fact
            return r;
        } catch (Exception e) {
            System.out.println("Operation error: " + e);
            e.printStackTrace();
            score.decrementScore();
        }
        return null;
    }

    Operation tryToGetOp(boolean random) {
        return EnvContext.getInstance().findRandomOperationFor(this, random);
    }

    Operation[] addRandomOperation(Operation[] ops) {
        Operation o = tryToGetOp(ops.length != 0);
        if (o != null) {
            Operation[] result = Arrays.copyOf(ops, ops.length + 1);
            result[result.length - 1] = o;
            return result;
        }
        return ops;
    }

    @Override
    public String toString() {
        return "BasicOp{" +
                "id=" + id +
                ", score=" + score +
                ", dead=" + dead +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BasicOp basicOp = (BasicOp) o;
        return id == basicOp.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
