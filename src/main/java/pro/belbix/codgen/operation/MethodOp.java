package pro.belbix.codgen.operation;

import java.util.Arrays;

public class MethodOp extends BasicOp {
    private Operation[] operations = new Operation[0];

    @Override
    public Object handle(Object o) {
        operations = removeDead(operations);
        operations = addRandomOperation(operations);

        Object[] result = new Object[operations.length];
        int i = 0;
        for (Operation op : operations) {
            result[i] = op.doOp(o);
            i++;
        }
        return routResult(result, o);
    }

    private Object routResult(Object result, Object o) {
        if (result == null) {
            return null;
        } else if (result instanceof Operation) {
            return ((Operation) result).doOp(o);
        } else if (result instanceof Operation[]) {
            Operation[] nextOps = (Operation[]) result;
            Object[] results = new Object[nextOps.length];
            int i = 0;
            for (Operation op : nextOps) {
                results[i] = op.doOp(o);
                i++;
            }
            return results;
        } else if (result instanceof double[]) {
            return result;
        } else if (result instanceof Double) {
            return result;
        } else if (result instanceof Object[]) {
            Object[] nextResults = (Object[]) result;
            Object[] results = new Object[nextResults.length];
            int i = 0;
            for (Object r : nextResults) {
                results[i] = routResult(r, o);
            }
            return results;
        }
        throw new IllegalStateException("Not supported result: " + result.getClass());
    }

    @Override
    public Operation[] getOperations() {
        return operations;
    }

    public void addOperation(Operation o) {
        operations = Arrays.copyOf(operations, operations.length + 1);
        operations[operations.length - 1] = o;
    }
}
