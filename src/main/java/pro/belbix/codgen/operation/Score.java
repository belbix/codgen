package pro.belbix.codgen.operation;

import lombok.Data;

@Data
public class Score {

    private final int distance;
    private int score = 0;

    public Score(int distance) {
        this.distance = distance;
    }

    public void incrementScore() {
        score++;
    }

    public void decrementScore() {
        score--;
    }
}
