package pro.belbix.codgen.operation;

public interface Operation {

    long getId();

    Object doOp(Object o);

    Score getScore();

    void markDead();

    boolean isDead();

    Operation[] getOperations();

}
