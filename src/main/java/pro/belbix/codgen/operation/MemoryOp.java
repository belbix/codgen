package pro.belbix.codgen.operation;

public class MemoryOp extends BasicOp {
    private double var;

    @Override
    public Object handle(Object o) {
        if (o != null) {
            readInput(o);
        }
        return var;
    }

    private void readInput(Object o) {
        if (o instanceof double[]) {
            double[] arr = (double[]) o;
            var = arr[0];
        } else {
            throw new IllegalStateException("Unsupported type " + o.getClass());
        }
    }

    public double getVar() {
        return var;
    }

    @Override
    public Operation[] getOperations() {
        return new Operation[0];
    }
}
