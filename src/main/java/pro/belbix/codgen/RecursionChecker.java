package pro.belbix.codgen;

import pro.belbix.codgen.operation.Operation;

import java.util.*;

public class RecursionChecker {
    private final Map<Long, Set<Long>> links = new HashMap<>();
    private final Map<Long, Set<Long>> callers = new HashMap<>();

    public static boolean manualRecursionCheck(List<Operation> ops) {
        boolean r;
        for (Operation o : ops) {
            r = manualRecursionCheck(o, o.getId());
            if (r) {
                return true;
            }
        }
        return false;
    }

    public static boolean manualRecursionCheck(Operation o, long startId) {
        for (Operation nextOp : o.getOperations()) {
            if (nextOp.getId() == startId) {
                System.out.println("WARN RECURSION in " + o.getId());
                return true;
            }
            boolean nextR = manualRecursionCheck(nextOp, startId);
            if (nextR) {
                return true;
            }
        }
        return false;
    }

    public void clear() {
        links.clear();
        callers.clear();
    }

    //if we find ID in links, add new ids (now it's chain call)
//    private void updatePrevLinks(Long id, Set<Long> newIds) {
//        if (Thread.currentThread().getStackTrace().length > 100) {
//            System.out.println("WARN large stack trace");
//        }
//        for (Map.Entry<Long, Set<Long>> e : links.entrySet()) {
//            Set<Long> prevLinks = e.getValue();
//            if (prevLinks.contains(id)) {
//                for (Long newId : newIds) {
//                    if (newId.equals(e.getKey())) {
//                        continue;
//                    }
//                    prevLinks.add(newId);
//                }
//                updatePrevLinks(e.getKey(), newIds);
//            }
//        }
//    }

    public boolean isRecursion(Operation searcher, Operation newOp) {
        //exclude duplicate call
        for (Operation o : searcher.getOperations()) {
            if (o.equals(newOp)) {
                return true;
            }
        }

        Set<Long> ids = links.get(newOp.getId());
        if (ids == null) {
            return false;
        }
        return ids.contains(searcher.getId());
    }

    public void removeFromCache(Long id) {
        links.remove(id);
        for (Set<Long> ids : links.values()) {
            ids.remove(id);
        }
    }

    public void updateLinks(Operation o, Operation f) {
        if (f == null) {
            return;
        }

        //obviously, add a link for new op F
        Set<Long> nextOpIds = links.computeIfAbsent(o.getId(), k -> new HashSet<>());
        nextOpIds.add(f.getId());

        //ADD NEXT because we will have a link to F. We should add all of them
        Set<Long> nextOpIdsForF = links.computeIfAbsent(f.getId(), k -> new HashSet<>());
        nextOpIds.addAll(nextOpIdsForF);

        //add a new caller for new OP
        Set<Long> nextOpCallersId = callers.computeIfAbsent(f.getId(), k -> new HashSet<>());
        nextOpCallersId.add(o.getId());
        //also add all callers of current OP as callers for new OP
        Set<Long> opCallersId = callers.computeIfAbsent(o.getId(), k -> new HashSet<>());
        nextOpCallersId.addAll(opCallersId);

        //ADD new links TO PREVIOUS
        Set<Long> p = new HashSet<>(nextOpIdsForF);
        p.add(f.getId());
        updatePrevLinks(o.getId(), p);
    }

    //if we find ID in links, add new ids (now it's chain call)
    private void updatePrevLinks(Long id, Set<Long> newIds) {
        for (Long callersId : callers.get(id)) {
            Set<Long> p = links.get(callersId);
            for (Long newId : newIds) {
                if (newId.equals(callersId)) {
                    continue;
                }
                p.add(newId);
            }
        }
    }

}
