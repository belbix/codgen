package pro.belbix.codgen;

import java.util.Random;

public class RandomUtil {
    private static final Random random = new Random();

    public static int randomInt(int max) {
        if (max == 0) {
            return 0;
        }
        return random.nextInt(max + 1);
    }

}
