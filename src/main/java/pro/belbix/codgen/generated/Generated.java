package pro.belbix.codgen.generated;

import java.util.ArrayList;
import java.util.List;

public class Generated {
    private Object var1;
    private Object var6;
    private Object var4;
    private Object var7;
    private Object var5;
    private Object var2;
    private Object var3;

    public Object method10(Object methodInput) {
        List<Object> result = new ArrayList<>();
        result.add(resolveVar4(methodInput));
        result.add(resolveVar4(methodInput));
        return result.toArray();
    }

    public Object method9(Object methodInput) {
        List<Object> result = new ArrayList<>();
        result.add(resolveVar1(methodInput));
        result.add( method10(methodInput));
        result.add(resolveVar1(methodInput));
        return result.toArray();
    }

    public Object method8(Object methodInput) {
        List<Object> result = new ArrayList<>();
        result.add(resolveVar3(methodInput));
        result.add(resolveVar3(methodInput));
        result.add( method9(methodInput));
        result.add(resolveVar3(methodInput));
        result.add(resolveVar3(methodInput));
        return result.toArray();
    }

    public Object method7(Object methodInput) {
        List<Object> result = new ArrayList<>();
        result.add( method8(methodInput));
        result.add(logic3(methodInput));
        result.add(resolveVar3(methodInput));
        result.add(logic3(methodInput));
        result.add(logic3(methodInput));
        return result.toArray();
    }

    public Object method6(Object methodInput) {
        List<Object> result = new ArrayList<>();
        result.add(resolveVar2(methodInput));
        result.add(resolveVar2(methodInput));
        result.add( method7(methodInput));
        result.add( method7(methodInput));
        result.add(resolveVar2(methodInput));
        return result.toArray();
    }

    public Object method5(Object methodInput) {
        List<Object> result = new ArrayList<>();
        result.add( method6(methodInput));
        result.add( method6(methodInput));
        result.add(resolveVar2(methodInput));
        return result.toArray();
    }

    public Object method4(Object methodInput) {
        List<Object> result = new ArrayList<>();
        result.add( method5(methodInput));
        result.add( method5(methodInput));
        return result.toArray();
    }

    public Object method3(Object methodInput) {
        List<Object> result = new ArrayList<>();
        result.add( method4(methodInput));
        result.add(resolveVar6(methodInput));
        return result.toArray();
    }

    public Object method2(Object methodInput) {
        List<Object> result = new ArrayList<>();
        result.add( method3(methodInput));
        result.add(resolveVar7(methodInput));
        return result.toArray();
    }

    public Object method11(Object methodInput) {
        List<Object> result = new ArrayList<>();
        return result.toArray();
    }

    public Object method1(Object methodInput) {
        List<Object> result = new ArrayList<>();
        result.add(logic1(methodInput));
        result.add(resolveVar1(methodInput));
        result.add(resolveVar7(methodInput));
        result.add(logic1(methodInput));
        result.add(logic1(methodInput));
        result.add(logic1(methodInput));
        result.add(resolveVar7(methodInput));
        result.add(logic1(methodInput));
        result.add( method2(methodInput));
        result.add(resolveVar7(methodInput));
        return result.toArray();
    }

    public Object logic2(Object methodInput) {
        List<Object> result = new ArrayList<>();
        double[] arr = (double[])methodInput;
        double a = arr[0];
        double b = arr[1];
        if (a == b) {
            return result.toArray();
        }
        return null;
    }

    public Object logic4(Object methodInput) {
        List<Object> result = new ArrayList<>();
        double[] arr = (double[])methodInput;
        double a = arr[0];
        double b = arr[1];
        if (a > b) {
            return result.toArray();
        }
        return null;
    }

    public Object logic3(Object methodInput) {
        List<Object> result = new ArrayList<>();
        double[] arr = (double[])methodInput;
        double a = arr[0];
        double b = arr[1];
        if (a < b) {
            result.add( method8(methodInput));
            result.add(logic4(methodInput));
            result.add(resolveVar5(methodInput));
            result.add(resolveVar5(methodInput));
            result.add( method8(methodInput));
            result.add( method8(methodInput));
            result.add(resolveVar5(methodInput));
            return result.toArray();
        }
        return null;
    }

    public Object logic1(Object methodInput) {
        List<Object> result = new ArrayList<>();
        double[] arr = (double[])methodInput;
        double a = arr[0];
        double b = arr[1];
        if (a < b) {
            result.add(resolveVar1(methodInput));
            result.add(logic2(methodInput));
            result.add( method2(methodInput));
            result.add(resolveVar7(methodInput));
            result.add(resolveVar7(methodInput));
            result.add( method2(methodInput));
            result.add( method2(methodInput));
            result.add(logic2(methodInput));
            result.add(resolveVar7(methodInput));
            result.add(logic2(methodInput));
            result.add( method2(methodInput));
            result.add(resolveVar7(methodInput));
            result.add(logic2(methodInput));
            result.add( method2(methodInput));
            result.add( method2(methodInput));
            result.add(logic2(methodInput));
            result.add(resolveVar7(methodInput));
            result.add(logic2(methodInput));
            result.add( method2(methodInput));
            result.add(resolveVar7(methodInput));
            result.add(logic2(methodInput));
            result.add( method2(methodInput));
            result.add(logic2(methodInput));
            result.add( method2(methodInput));
            result.add(resolveVar7(methodInput));
            result.add(logic2(methodInput));
            result.add(resolveVar7(methodInput));
            result.add( method11(methodInput));
            result.add(logic2(methodInput));
            result.add(logic2(methodInput));
            result.add(logic2(methodInput));
            return result.toArray();
        }
        return null;
    }

    public Object resolveVar1(Object methodInput) {
        if (methodInput!= null) {
            double v = ((double[])methodInput)[0];
            this.var1 = v;
        }
        return this.var1;
    }

    public Object resolveVar6(Object methodInput) {
        if (methodInput!= null) {
            double v = ((double[])methodInput)[0];
            this.var6 = v;
        }
        return this.var6;
    }

    public Object resolveVar4(Object methodInput) {
        if (methodInput!= null) {
            double v = ((double[])methodInput)[0];
            this.var4 = v;
        }
        return this.var4;
    }

    public Object resolveVar7(Object methodInput) {
        if (methodInput!= null) {
            double v = ((double[])methodInput)[0];
            this.var7 = v;
        }
        return this.var7;
    }

    public Object resolveVar5(Object methodInput) {
        if (methodInput!= null) {
            double v = ((double[])methodInput)[0];
            this.var5 = v;
        }
        return this.var5;
    }

    public Object resolveVar2(Object methodInput) {
        if (methodInput!= null) {
            double v = ((double[])methodInput)[0];
            this.var2 = v;
        }
        return this.var2;
    }

    public Object resolveVar3(Object methodInput) {
        if (methodInput!= null) {
            double v = ((double[])methodInput)[0];
            this.var3 = v;
        }
        return this.var3;
    }

}
