package pro.belbix.codgen.print;

import pro.belbix.codgen.operation.MethodOp;

import static pro.belbix.codgen.print.MemoryPrinter.varResolverName;
import static pro.belbix.codgen.print.PrintContext.*;

public class SourcePrinter {

    private final PrintContext printContext;

    public SourcePrinter() {
        this.printContext = new PrintContext();
    }

    public String print(MethodOp methodOp) {
        new MethodPrinter(printContext).printMethod(methodOp);
        return "package pro.belbix.codgen.generated;\n\n" +
                "import java.util.ArrayList;\n" +
                "import java.util.List;\n\n" +
                "public class Generated {\n" +
                printVarsDefinitions() +
                printBasicMethods() +
                printLogicMethods() +
                printVarsMethods() +
                "\n}";
    }

    private String printBasicMethods() {
        StringBuilder sb = new StringBuilder();
        for (String def : printContext.getMethods().values()) {
            sb.append(def);
        }
        return sb.toString();
    }

    private String printLogicMethods() {
        StringBuilder sb = new StringBuilder();
        for (String def : printContext.getLogic().values()) {
            sb.append(def);
        }
        return sb.toString();
    }

    private String printVarsMethods() {
        StringBuilder sb = new StringBuilder();
        for (String name : printContext.getVariableNames().values()) {
            sb.append(startOfMethod(varResolverName(name)))
                    .append(INDENT2 + "if (" + METHOD_INPUT + "!= null) {\n")
                    .append(INDENT3 + "double v = ((double[])" + METHOD_INPUT + ")[0];\n")
                    .append(INDENT3)
                    .append("this.")
                    .append(name)
                    .append(" = v;\n")
                    .append(INDENT2 + "}\n")
                    .append(INDENT2 + "return this.")
                    .append(name).append(";\n")
                    .append(INDENT + "}\n");
        }


        return sb.toString();
    }


    private String printVarsDefinitions() {
        StringBuilder sb = new StringBuilder();
        for (String name : printContext.getVariableNames().values()) {
            sb.append(INDENT + "private Object ").append(name).append(";\n");
        }
        return sb.toString();
    }


}
