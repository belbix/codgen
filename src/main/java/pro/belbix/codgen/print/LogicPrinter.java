package pro.belbix.codgen.print;

import pro.belbix.codgen.operation.LogicOp;
import pro.belbix.codgen.operation.Operation;

import java.util.Arrays;

import static pro.belbix.codgen.print.PrintContext.*;

public class LogicPrinter {

    private final PrintContext printContext;

    public LogicPrinter(PrintContext printContext) {
        this.printContext = printContext;
    }

    String printLogic(LogicOp logicOp) {
        StringBuilder sb = new StringBuilder();

        String name = printContext.getLogicNames().get(logicOp.getId());
        if (name == null) {
            name = "logic" + printContext.getCountOfLogic().incrementAndGet();
            printContext.getLogicNames().put(logicOp.getId(), name);
        }

        if (!printContext.getLogic().containsKey(logicOp.getId())) {
            printContext.getLogic().put(logicOp.getId(), printLogicMethod(logicOp));
        }

        sb.append(INDENT3)
                .append("result.add(")
                .append(name)
                .append("(")
                .append(METHOD_INPUT)
                .append("));\n");

        return sb.toString();
    }

    private String printLogicMethod(LogicOp logicOp) {
        StringBuilder sb = new StringBuilder();
        sb.append(startOfMethod(printContext.getLogicNames().get(logicOp.getId())))
                .append(methodResultVar())
                .append(startOfLogic(logicOp));
        for (Operation op : logicOp.getOperations()) {
            if (op == null) {
                throw new IllegalStateException("op is null: " + Arrays.toString(logicOp.getOperations()));
            }
            if (op.equals(logicOp)) {
                sb.append("//recursive call ").append(op.getId());
                continue;
            }
            try {
                sb.append(printContext.printOperation(op));
            } catch (StackOverflowError e) {
                sb.append("//stack over flow ").append(op.getId());
            } catch (Exception e) {
                sb.append("//err ").append(e.getMessage());
            }

        }
        sb.append(methodResultReturn())
                .append(INDENT2 + "}\n")
                .append(INDENT2 + "return null;\n")
                .append(INDENT + "}\n");
        return sb.toString();
    }

    private String startOfLogic(LogicOp logicOp) {
        return INDENT2 + "double[] arr = (double[])" + METHOD_INPUT + ";\n" +
                INDENT2 + "double a = arr[0];\n" +
                INDENT2 + "double b = arr[1];\n" +
                INDENT2 + "if (a " + logicOp.getLogic().getValue() + " b) {\n";
    }
}
