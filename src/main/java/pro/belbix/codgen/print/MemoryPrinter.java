package pro.belbix.codgen.print;

import pro.belbix.codgen.operation.MemoryOp;

import static pro.belbix.codgen.print.PrintContext.*;

public class MemoryPrinter {

    private final PrintContext printContext;

    public MemoryPrinter(PrintContext printContext) {
        this.printContext = printContext;
    }

    String printMemory(MemoryOp memoryOp) {
        StringBuilder sb = new StringBuilder();

        String name = printContext.getVariableNames().get(memoryOp.getId());
        if (name == null) {
            name = "var" + printContext.getCountOfVars().incrementAndGet();
            printContext.getVariableNames().put(memoryOp.getId(), name);
        }

        sb.append(INDENT2 + "result.add(")
                .append(varResolverName(name))
                .append("(")
                .append(METHOD_INPUT)
                .append("));\n");

        return sb.toString();
    }

    public static String varResolverName(String name) {
        String firstChar = name.substring(0, 1);
        return "resolve" + name.replaceFirst(firstChar, firstChar.toUpperCase());
    }
}
