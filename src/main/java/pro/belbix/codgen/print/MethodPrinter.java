package pro.belbix.codgen.print;

import pro.belbix.codgen.operation.MethodOp;
import pro.belbix.codgen.operation.Operation;

import static pro.belbix.codgen.print.PrintContext.*;

public class MethodPrinter {

    private final PrintContext printContext;

    public MethodPrinter(PrintContext printContext) {
        this.printContext = printContext;
    }

    void printMethod(MethodOp methodOp) {
        if (printContext.getMethods().containsKey(methodOp.getId())) {
            return;
        }

        String name = printContext.resolveMethodName(methodOp);

        StringBuilder sb = new StringBuilder();
        sb.append(startOfMethod(name))
                .append(methodResultVar());

        for (Operation operation : methodOp.getOperations()) {
            sb.append(printContext.printOperation(operation));
        }

        sb.append(methodResultReturn())
                .append(INDENT + "}\n");
        printContext.getMethods().put(methodOp.getId(), sb.toString());
    }

    String printCallMethod(MethodOp methodOp) {
        printMethod(methodOp);
        String methodName = printContext.resolveMethodName(methodOp);
        return INDENT3 + "result.add( " + methodName + "(" + METHOD_INPUT + "));\n";
    }

}
