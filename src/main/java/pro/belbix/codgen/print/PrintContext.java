package pro.belbix.codgen.print;

import lombok.Data;
import pro.belbix.codgen.operation.LogicOp;
import pro.belbix.codgen.operation.MemoryOp;
import pro.belbix.codgen.operation.MethodOp;
import pro.belbix.codgen.operation.Operation;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

@Data
public class PrintContext {
    final static String METHOD_INPUT = "methodInput";
    final static String INDENT = "    ";
    final static String INDENT2 = INDENT + INDENT;
    final static String INDENT3 = INDENT2 + INDENT;

    private final Map<Long, String> methods = new LinkedHashMap<>();
    private final Map<Long, String> methodNames = new HashMap<>();

    private final Map<Long, String> variableNames = new HashMap<>();

    private final Map<Long, String> logic = new LinkedHashMap<>();
    private final Map<Long, String> logicNames = new HashMap<>();

    private final AtomicInteger countOfMethod = new AtomicInteger(0);
    private final AtomicInteger countOfVars = new AtomicInteger(0);
    private final AtomicInteger countOfLogic = new AtomicInteger(0);

    String printOperation(Operation operation) {
        if (operation instanceof MethodOp) {
            return new MethodPrinter(this).printCallMethod((MethodOp) operation);
        } else if (operation instanceof LogicOp) {
            return new LogicPrinter(this).printLogic((LogicOp) operation);
        } else if (operation instanceof MemoryOp) {
            return new MemoryPrinter(this).printMemory((MemoryOp) operation);
        }
        throw new IllegalStateException("Unsupported operation: " + operation.getClass());
    }

    String resolveMethodName(MethodOp methodOp) {
        String methodName = methodNames.get(methodOp.getId());
        if (methodName == null) {
            methodName = "method" + countOfMethod.incrementAndGet();
            methodNames.put(methodOp.getId(), methodName);
        }
        return methodName;
    }

    static String startOfMethod(String name) {
        return "\n" + INDENT + "public Object " + name + "(Object " + METHOD_INPUT + ") {\n";
    }

    static String methodResultVar() {
        return INDENT2 + "List<Object> result = new ArrayList<>();\n";
    }

    static String methodResultReturn() {
        return INDENT2 + "return result.toArray();\n";
    }

}
