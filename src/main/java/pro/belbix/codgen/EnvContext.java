package pro.belbix.codgen;

import pro.belbix.codgen.operation.*;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static pro.belbix.codgen.RandomUtil.randomInt;

public class EnvContext {
    private static final int INIT_COUNT = 1000000;
    private static final int MAX_HOLD = INIT_COUNT;
    private static final int MAX_LINK_SIZE = 100;
    private static final int MAX_DISTANCE = 100_000;
    private static final int CHANCE_FIND_OPERATION = 0;
    private static final int CHANCE_ADD_OPERATION = Integer.MAX_VALUE / 10;

    private static final EnvContext instance = new EnvContext();
    private static final Comparator<Operation> SCORE_COMPARATOR = Comparator.comparingInt(o -> o.getScore().getScore());
    private final List<Operation> logic = new ArrayList<>();
    private final List<Operation> memory = new ArrayList<>();
    private final List<Operation> methods = new ArrayList<>();
    private final RecursionChecker recursionChecker = new RecursionChecker();


    private EnvContext() {
    }

    public static EnvContext getInstance() {
        return instance;
    }

    public void initOps() {
        int c = 0;
        while (c < INIT_COUNT) {
            addLogic(new LogicOp());
            addMemory(new MemoryOp());
            addMethod(new MethodOp());
            c++;
        }
    }

    public void clear() {
        logic.clear();
        memory.clear();
        methods.clear();
        recursionChecker.clear();
    }

    public RecursionChecker getRecursionChecker() {
        return recursionChecker;
    }

    public Operation findRandomOperationFor(Operation o, boolean random) {
        if ((random && randomInt(CHANCE_FIND_OPERATION) != 0)
                || o.getOperations().length > MAX_LINK_SIZE) {
            return null;
        }
        if (logic.isEmpty() || memory.isEmpty() || methods.isEmpty() || randomInt(CHANCE_ADD_OPERATION) == 0) {
            addRandomOp();
        }
        Operation f = getBestFor(o, randomInt(2));
        if (f != null) {
            Instant start = Instant.now();
            recursionChecker.updateLinks(o, f);
            long sec = Duration.between(start, Instant.now()).toSeconds();
//            if(sec > 10) {
//                System.out.println("too long");
//            }
            System.out.println(o.getId() + " -> " + f.getId() + " added by " + sec);
        }

//        manualRecursionCheck(logic);
//        manualRecursionCheck(methods);

        return f;
    }

    private void manualRecursionCheck(List<Operation> ops) {
        if (RecursionChecker.manualRecursionCheck(ops)) {
            System.out.println("Recursion!");
        }
    }

    /**
     * 0 - logc, 1 - memory, 2 - method
     */
    public Operation getBestFor(Operation o, int type) {
        switch (type) {
            case 0: //logic
                return findBest(logic, o);
            case 1: //memory
                return findBest(memory, o);
            case 2: //method
                return findBest(methods, o);
        }
        return null;
    }


    private void addRandomOp() {
        switch (randomInt(2)) {
            case 0: //logic
                addLogic(new LogicOp());
                break;
            case 1: //memory
                addMemory(new MemoryOp());
                break;
            case 2: //method
                addMethod(new MethodOp());
                break;
        }
    }

    public void addLogic(Operation o) {
        addOp(o, logic);
    }

    public void addMemory(Operation o) {
        addOp(o, memory);
    }

    public void addMethod(Operation o) {
        addOp(o, methods);
    }

    private void addOp(Operation o, List<Operation> ops) {
        ops.add(o);
        clearExcess(ops);
    }

    private void clearExcess(List<Operation> ops) {
        while (ops.size() > MAX_HOLD) {
            ops.sort(SCORE_COMPARATOR);
            Operation removed = ops.remove(0);
            if (removed != null) {
                removed.markDead();
                recursionChecker.removeFromCache(removed.getId());
            }
        }
    }

    private Operation findBest(List<Operation> ops, Operation searcher) {
        ops.sort(Comparator.comparing(o -> calcScore(((Operation) o).getScore(), searcher.getScore().getDistance()))
                .reversed());
        for (Operation o : ops) {
            if (o.equals(searcher)
                    || recursionChecker.isRecursion(searcher, o)
            ) {
                continue;
            }
            return o;
        }
        return null;
    }

    private static int calcScore(Score score, int distance) {
        int d = Math.abs(score.getDistance() - distance);
        return score.getScore() - d;
    }

}
