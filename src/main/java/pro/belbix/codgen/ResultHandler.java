package pro.belbix.codgen;

import java.util.Arrays;
import java.util.function.Consumer;

public class ResultHandler {

    public static void handleResult(Object o, Consumer<String> handler) {
        if (o == null) {
            handler.accept("Result:\nnull");
        } else if (o instanceof double[]) {
            handler.accept("Result:\n" + Arrays.toString((double[]) o));
        } else if (o instanceof Object[]) {
            Object[] resultObjects = (Object[]) o;
            for (Object resultO : resultObjects) {
                handleResult(resultO, handler);
            }
        } else if (o instanceof Double) {
            handler.accept("Result:\n" + o);
        } else {
            throw new IllegalStateException("Unsupported object class: " + o.getClass());
        }
    }
}
